FROM debian:jessie-slim

RUN apt-get update && \
apt-get install -y build-essential perl cpanminus libtemplate-perl libdancer2-perl libxml-libxml-perl && \
	rm -rf /var/lib/apt/lists/* && \
	cpanm File::Find Carp Number::Format Data::Dumper DateTime Dancer2::Plugin::Ajax Plack LWP::UserAgent HTTP::Request URI::Escape

ENTRYPOINT ["/bin/sh", "-c"]
